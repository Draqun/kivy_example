#!/usr/bin/env bash

sudo add-apt-repository ppa:kivy-team/kivy
sudo add-apt-repository ppa:mc3man/trusty-media

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A863D2D6
sudo apt update
sudo apt install -y \
    python3-virtualenv \
    python3.6 \
    git \
    mercurial \
    python-kivy \
    ffmpeg \
    python-pip \
    build-essential \
    python3-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev \
    libgstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    build-essential \
    ccache \
    libncurses5:i386 \
    libstdc++6:i386 \
    libgtk2.0-0:i386 \
    libpangox-1.0-0:i386 \
    libpangoxft-1.0-0:i386 \
    libidn11:i386 \
    unzip \
    zlib1g:i386 \
    openjdk-8-jdk \
    openjdk-8-jre \
    autotools-dev \
    autoconf \
    automake \
    libtool \
    libltdl-dev

sudo ln -s /usr/lib/jvm/java-1.8.0-openjdk-amd64/ /usr/lib/jvm/default-java
echo "export JAVA_HOME=/usr/lib/jvm/default-java"  >> ~/.bashrc
echo "export JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'" >> ~/.bashrc

python -m virtualenv --python=/usr/bin/python3.6 ./venv/

source ./venv/bin/activate

./venv/bin/pip install -r requirements.txt
./venv/bin/pip install hg+http://bitbucket.org/pygame/pygame
./venv/bin/pip install kivy
